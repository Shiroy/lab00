terraform {
  backend "s3" {
    region = "eu-west-3"
    bucket = "tfstateawacheuxd2si"
    key = "webapp/terraform.tfstate"
    dynamodb_table = "terraform_locks"
  }
}
