data "terraform_remote_state" "webapp" {
  backend = "s3"
  config {
    region = "eu-west-3"
    bucket = "tfstateawacheuxd2si"
    key = "vpc/terraform.tfstate"
  }
}

